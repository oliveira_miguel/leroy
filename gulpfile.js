process.env.DISABLE_NOTIFIER = true;
var elixir = require('laravel-elixir');

elixir(function(mix) {
    mix.scripts([
        './node_modules/jquery/dist/jquery.min.js',
        './node_modules/garden/dist/js/garden.min.js'
    ]).

    styles([
        './node_modules/garden/dist/css/garden.min.css',
        './resources/assets/css/**.css'
    ]).

    copy('./node_modules/garden/dist/fonts/**.*', './public/fonts/');
});