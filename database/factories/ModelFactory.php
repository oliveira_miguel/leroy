<?php

$factory->define(App\Models\Product::class, function (Faker\Generator $faker) {
    return [
        'code' => rand(1, 9999999),
        'name' => $faker->text(20),
        'free_shipping' => $faker->boolean(),
        'description' => $faker->text(30),
        'price' => $faker->randomNumber(2),
        'category' => $faker->text(10)
    ];
});