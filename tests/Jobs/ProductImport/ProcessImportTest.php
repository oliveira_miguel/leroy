<?php

use App\Jobs\ProductImport\ProcessImport;
use App\Jobs\ProductImport\Reader;
use App\Models\Product;
use App\Repositories\ProductRepository;
use Mockery as m;

class ProcessImportTest extends TestCase
{
    public function setUp()
    {
        parent::setUp();
    }

    public function test_should_skip_all_invalid_data()
    {
        $data = [
            'code' => 'Invalid Code',
            'name' => 'Missing category',
            'free_shipping' => 'Invalid shipping',
            'description' => 'Missing category',
            'price' => 'Invalid Price'
        ];
        $invalidData = collect();
        $invalidData->push($data);

        $reader = m::mock(Reader::class);
        $reader->shouldReceive('getData')->andReturn($invalidData);

        $repository = m::mock(ProductRepository::class);
        $repository->shouldReceive('findByCode')->never()
                   ->shouldReceive('store')->never()
                   ->shouldReceive('update')->never();

        $process = new ProcessImport($repository, $reader);
        $process->handle();
    }

    public function test_should_store_when_product_does_not_exist()
    {
        $data = [
            'code' => 1000,
            'name' => 'Test',
            'free_shipping' => 1,
            'description' => 'Test',
            'price' => 100,
            'category' => 'Test'
        ];
        $collection = collect();
        $collection->push($data);

        $reader = m::mock(Reader::class);
        $reader->shouldReceive('getData')->andReturn($collection);

        $repository = m::mock(ProductRepository::class);
        $repository->shouldReceive('findByCode')->with($data['code'])->once()->andReturnNull()
                    ->shouldReceive('store')->with($data)->once();

        $process = new ProcessImport($repository, $reader);
        $process->handle();
    }

    public function test_should_update_when_product_exist()
    {
        $data = [
            'code' => 1000,
            'name' => 'Test',
            'free_shipping' => 1,
            'description' => 'Test',
            'price' => 100,
            'category' => 'Test'
        ];
        $collection = collect();
        $collection->push($data);

        $reader = m::mock(Reader::class);
        $reader->shouldReceive('getData')->andReturn($collection);

        $product = new Product($data);
        $product->id = 1;

        $repository = m::mock(ProductRepository::class);
        $repository->shouldReceive('findByCode')->with($data['code'])->once()->andReturn($product)
                    ->shouldReceive('update')->with($product->id, $data)->once();

        $process = new ProcessImport($repository, $reader);
        $process->handle();
    }

    public function tearDown()
    {
        m::close();
    }
}
