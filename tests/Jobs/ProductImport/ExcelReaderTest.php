<?php

use App\Jobs\ProductImport\ExcelReader;
use Mockery as m;

class ExcelReaderTest extends TestCase
{
    public function setUp()
    {
        parent::setUp();
    }

    public function test_read_xlsx_file()
    {
        $collection = collect();
        $collection->push([
            'code' => '1001',
            'name' => 'product name 1',
            'free_shipping' => true,
            'description' => 'product description 1',
            'price' => 100.00,
            'category' => 'category'
        ]);

        $collection->push([
            'code' => '1002',
            'name' => 'product name 2',
            'free_shipping' => false,
            'description' => 'product description 2',
            'price' => 999.12,
            'category' => 'category'
        ]);

        $filePath = __DIR__ . '/files/products.xlsx' ;

        $reader = new ExcelReader($filePath);
        $data = $reader->getData();

        $this->assertEquals($data, $collection);
    }

    public function test_read_empty_xml_return_empty_collection()
    {
        $collection = collect();
        $filePath = __DIR__ . '/files/products_empty.xlsx' ;
        $reader = new ExcelReader($filePath);
        $data = $reader->getData();

        $this->assertEquals($data, $collection);
    }
}