<?php

use App\Models\Product;
use App\Repositories\ProductRepository;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Support\Facades\App;

class ProductControllerTest extends TestCase
{
    use DatabaseMigrations;

    public function setUp()
    {
        parent::setUp();
    }
    
    public function test_index()
    {
        factory(Product::class, 1)->create();
        $this->route('GET', 'products.index');
        $this->assertViewHas('products');
    }

    public function test_edit()
    {
        factory(Product::class, 1)->create();

        $repository = App::make(ProductRepository::class);
        $product = $repository->find(1);

        $this->route('GET', 'products.edit', ['products' => 1]);
        $this->assertViewHas('product', $product);
    }

    public function test_update()
    {
        factory(Product::class, 1)->create();

        $data = [
            'name' => 'Test',
            'category' => 'Test',
            'price' => 100,
            'description' => 'Test',
            'free_shipping' => true
        ];

        $this->route('PUT', 'products.update', ['products' => 1], $data);
        $this->seeInDatabase('products', $data);
        $this->assertRedirectedToRoute('products.index');
    }

    public function test_destroy()
    {
        factory(Product::class, 1)->create();

        $this->route('DELETE', 'products.destroy', ['products' => 1]);
        $this->missingFromDatabase('products', ['id' => 1, 'deleted_at' => null]);
        $this->assertRedirectedToRoute('products.index');
    }
}