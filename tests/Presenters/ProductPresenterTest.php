<?php

use Mockery as m;
use App\Models\Product;

class ProductPresenterTest extends TestCase
{
    public function setUp()
    {
        parent::setUp();
    }

    public function test_free_shipping_presenter()
    {
        $model = new Product();

        $model->free_shipping = 1;
        $this->assertEquals('Yes', $model->present()->freeShipping);

        $model->free_shipping = 0;
        $this->assertEquals('No', $model->present()->freeShipping);
    }

    public function test_price_presenter()
    {
        $model = new Product();

        $model->price = 1000.00;
        $this->assertEquals('1,000.00', $model->present()->pricePresent);

        $model->price = 100;
        $this->assertEquals('100.00', $model->present()->pricePresent);
    }
}