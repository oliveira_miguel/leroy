<?php

use Mockery as m;
use App\Repositories\ProductRepository;
use App\Models\Product;

class ProductRepositoryTest extends TestCase
{
    private $products;
    private $repository;
    private $model;

    public function setUp()
    {
        parent::setUp();

        $this->products = factory(App\Models\Product::class, 2)->make();

        $this->model = m::mock(Product::class);
        $this->repository = new ProductRepository($this->model);
    }

    public function test_retrieve_all_products()
    {
        $this->model->shouldReceive('all')
                    ->once()
                    ->andReturn($this->products);

        $products = $this->repository->all();

        $this->assertEquals($products, $this->products);
    }

    public function test_get_product_by_code()
    {
        $code = 100;

        $eloquentBuilder = $this->getEloquentBuilderMock();
        $this->configureWhereOnModel($eloquentBuilder, 'code', $code);

        $product = $this->repository->findByCode($code);
        $this->assertEquals($this->products[0], $product);
    }

    public function test_update_product()
    {
        $id = 1;
        $data = [
            'name' => 'Test',
            'category' => 'Test',
            'price' => 100,
            'description' => 'Test'
        ];

        $modelUpdate = m::mock(Product::class);
        $modelUpdate->shouldReceive('update')->with($data)->once();

        $eloquentBuilder = $this->getEloquentBuilderMock($modelUpdate);
        $this->configureWhereOnModel($eloquentBuilder, 'id', $id);

        $this->repository->update($id, $data);
    }

    public function test_store_product()
    {
        $data = [
            'name' => 'Test',
            'category' => 'Test',
            'price' => 100,
            'description' => 'Test'
        ];

        $modelUpdate = m::mock(Product::class);
        $modelUpdate->shouldReceive('fill')->with($data)->once()
                    ->shouldReceive('save')->once();
        
        $this->model->shouldReceive('replicate')
                    ->andReturn($modelUpdate);
        
        $product = $this->repository->store($data);
        
        $this->assertEquals($modelUpdate, $product);
    }

    public function test_update_return_null_when_model_not_found()
    {
        $id = 1;
        $data = [];

        $modelUpdate = m::mock(Product::class);
        $modelUpdate->shouldReceive('fill')->with($data)->never()
                    ->shouldReceive('save')->never();

        $eloquentBuilder = m::mock(Illuminate\Database\Eloquent\Builder::class);
        $eloquentBuilder->shouldReceive('first')->once()->andReturn(null);
        $this->configureWhereOnModel($eloquentBuilder, 'id', $id);

        $result = $this->repository->update($id, $data);

        $this->assertEquals(null, $result);
    }

    public function test_delete_product()
    {
        $id = 1;

        $modelDelete = m::mock(Product::class);
        $modelDelete->shouldReceive('delete')->once();

        $eloquentBuilder = $this->getEloquentBuilderMock($modelDelete);
        $this->configureWhereOnModel($eloquentBuilder, 'id', $id);

        $this->repository->delete($id);
    }

    public function test_delete_return_false_when_model_not_found()
    {
        $id = 1;

        $modelDelete = m::mock(Product::class);
        $modelDelete->shouldReceive('delete')->never();

        $eloquentBuilder = m::mock(Illuminate\Database\Eloquent\Builder::class);
        $eloquentBuilder->shouldReceive('first')->once()->andReturn(null);
        $this->configureWhereOnModel($eloquentBuilder, 'id', $id);

        $result = $this->repository->delete($id);

        $this->assertEquals(false, $result);
    }

    private function getEloquentBuilderMock(Product $return = null)
    {
        $productReturn = $return ? $return : $this->products[0];

        $eloquentBuilder = m::mock(Illuminate\Database\Eloquent\Builder::class);
        $eloquentBuilder->shouldReceive('first')->once()->andReturn($productReturn);

        return $eloquentBuilder;
    }

    private function configureWhereOnModel($eloquentBuilder, $column = 'id', $value = 1)
    {
        $this->model->shouldReceive('where')->with($column, $value)->once()->andReturn($eloquentBuilder);
    }

    public function tearDown()
    {
        m::close();
    }
}