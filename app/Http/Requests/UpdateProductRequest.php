<?php

namespace App\Http\Requests;

class UpdateProductRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name' => 'required|string|max:255',
            'free_shipping' => 'required|boolean',
            'description' => 'string|max:255',
            'price' => 'required|numeric|max:99999.999',
            'category' => 'required|string|max:255'
        ];
    }
}