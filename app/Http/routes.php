<?php
Route::group(['middleware' => ['web']], function () {
    Route::get('/', 'ProductController@index')->name('product.list');
    Route::resource('products', 'ProductController',  ['except' => ['create', 'store', 'show']]);

    Route::get('/import', 'ImportController@index')->name('import.index');
    Route::post('/import', 'ImportController@process')->name('import.process');
});