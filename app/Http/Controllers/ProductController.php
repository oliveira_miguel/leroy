<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Repositories\ProductRepository;
use App\Http\Requests\UpdateProductRequest;

class ProductController extends Controller
{
    private $repository;

    public function __construct(ProductRepository $repository)
    {
        $this->repository = $repository;
    }

    public function index()
    {
        $products = $this->repository->all();
        return view('product/index', ['products' => $products]);
    }

    public function edit(Product $products)
    {
        return view('product/form', ['product' => $products]);
    }

    public function update(UpdateProductRequest $request, $id)
    {
        $this->repository->update($id, $request->all());
        return redirect('products');
    }

    public function destroy($id)
    {
        $this->repository->delete($id);
        return redirect('products');
    }
}