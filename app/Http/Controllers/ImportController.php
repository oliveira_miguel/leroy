<?php

namespace App\Http\Controllers;

use App\Jobs\ProductImport\ExcelReader;
use App\Jobs\ProductImport\ProcessImport;
use App\Repositories\ProductRepository;
use Validator;
use Illuminate\Http\Request;

class ImportController extends Controller
{
    public function index()
    {
        return view('import/index');
    }

    public function process(Request $request, ProductRepository $repository)
    {
        $validator = Validator::make($request->all(), [
            'file' => 'required|mimes:xlsx|max:5000',
        ]);

        if ($validator->fails()) {
            return redirect('import')
                   ->withErrors($validator);
        }

        $file = $request->file('file');

        $fileName = uniqid() . '.xlsx';
        $file->move(storage_path('app/import'), $fileName);

        $filePath = storage_path('app/import/') . $fileName;
        
        $reader = new ExcelReader($filePath);
        $importJob = new ProcessImport($repository, $reader);
        $this->dispatch($importJob);

        return redirect('import')
               ->with('success', true);
    }
}