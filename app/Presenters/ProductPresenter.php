<?php

namespace App\Presenters;

use Laracasts\Presenter\Presenter;

class ProductPresenter extends Presenter
{
    public function freeShipping()
    {
        $freeShipping = 'Yes';

        if ( ! $this->free_shipping) {
            $freeShipping = 'No';
        }

        return $freeShipping;
    }

    public function pricePresent()
    {
        return number_format($this->price, 2);
    }
}