<?php

namespace App\Repositories;

class BaseRepository
{
    protected $model;

    public function find($value, $column = 'id')
    {
        return $this->model->where($column, $value)->first();
    }

    public function all()
    {
        return $this->model->all();
    }

    public function update($id, $data)
    {
        $model = $this->find($id);

        if (! $model) {
            return null;
        }

        $model->update($data);

        return $model;
    }

    public function store($data)
    {
        $model = $this->model->replicate();
        $model->fill($data);
        $model->save();

        return $model;
    }

    public function delete($id)
    {
        $model = $this->find($id);
        if (! $model) {
            return false;
        }

        $model->delete();

        return true;
    }
}