<?php

namespace App\Models;

use App\Presenters\ProductPresenter;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Laracasts\Presenter\PresentableTrait;

class Product extends Model
{
    use SoftDeletes;
    use PresentableTrait;

    protected $presenter = ProductPresenter::class;

    protected $fillable = [
        'code',
        'name',
        'free_shipping',
        'description',
        'price',
        'category'
    ];

    protected $dates = [
        'deleted_at'
    ];
}