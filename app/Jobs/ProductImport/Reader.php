<?php

namespace App\Jobs\ProductImport;

interface Reader
{
    public function getData();
}