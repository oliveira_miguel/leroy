<?php

namespace App\Jobs\ProductImport;

use Excel;
use PHPExcel_Exception;

class ExcelReader implements Reader
{
    private $filePath;

    public function __construct($filePath)
    {
        $this->filePath = $filePath;
    }

    public function getData()
    {
        $collection = collect();
        try {
            $sheet = Excel::selectSheetsByIndex(0)->load($this->filePath);
            $category = $sheet->getActiveSheet()->getCell('B2')->getValue();
            $data = $sheet->get();
        } catch (PHPExcel_Exception $e) {
            return $collection;
        }

        foreach ($data as $row) {
            $collection->push([
                'code' => $row->get('lm'),
                'name' => $row->get('name'),
                'free_shipping' => (bool) $row->get('free_shipping'),
                'description' => $row->get('description'),
                'price' => (float) $row->get('price'),
                'category' => $category
            ]);
        }

        return $collection;
    }
}