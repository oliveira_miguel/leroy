<?php

namespace App\Jobs\ProductImport;

use App\Http\Requests\UpdateProductRequest;
use App\Jobs\Job;
use App\Repositories\ProductRepository;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Validator;

class ProcessImport extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    private $repository;
    private $reader;

    public function __construct(
        ProductRepository $repository,
        Reader $reader
    ) {
        $this->reader = $reader;
        $this->repository = $repository;
    }

    public function handle()
    {
        $data = $this->reader->getData();
        foreach ($data as $productData) {
            $isValid = $this->isProductDataValid($productData);
            if ( ! $isValid) {
                continue;
            }

            $productReference = $this->repository->findByCode(
                array_get($productData ,'code')
            );
            if ( ! $productReference) {
                $this->repository->store($productData);
            } else {
                $this->repository->update($productReference->id, $productData);
            }
        }
    }

    private function isProductDataValid($productData)
    {
        $rules = new UpdateProductRequest();
        $validator = Validator::make($productData, $rules->rules());

        return $validator->passes();
    }
}
