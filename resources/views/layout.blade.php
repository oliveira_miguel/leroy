<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Leroy Merlin</title>
    <link href="{{ asset('favicon.ico') }}" type="image/x-icon" rel="icon"/>
    <link rel="stylesheet" href="{{ asset('css/all.css') }}">
</head>
<body>
<header class="header">
    <div class="container">
        <h1>Leroy Merlin</h1>
        <nav class="nav">
            <a href="{{ route('products.index') }}" class="link">List</a>
            <a href="{{ route('import.index') }}" class="link">Import</a>
        </nav>
    </div>
</header>

<div class="container">
    @yield('content')
</div>
<script src="{{ asset('js/all.js') }}"></script>
</body>
</html>