@extends('layout')

@section('content')
    <section class="row">
        <div class="col-xs-offset-3 col-xs-9">
            <h2>Import Products</h2>
            @if ($errors->has('file'))
                <div class="feedback feedback-error">
                    <div class="feedback-title">
                        Fail!
                    </div>
                    {!! $errors->first('file')  !!}
                </div>
            @elseif (Session::get('success') == true)
                <div class="feedback feedback">
                    <div class="feedback-title">
                        Success!
                    </div>
                    Your file will be processed as soon.
                </div>
            @endif

            {!! Form::open(['files' => true, 'route' => 'import.process', 'class' => 'import-form']) !!}
            {!! Form::file('file') !!}
            {!! Form::submit('Send File', ['class' => 'button button-primary']) !!}
            {!! Form::close() !!}
        </div>
    </section>
@endsection