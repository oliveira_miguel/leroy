@extends('layout')

@section('content')
    <section class="row">
        <h2>Products</h2>
        <div class="col-xs-12">
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th>Code</th>
                        <th>Name</th>
                        <th>Free Shipping</th>
                        <th>Description</th>
                        <th>Price</th>
                        <th>Category</th>
                        <th width="160px">Actions</th>
                    </tr>
                </thead>
                <tbody>
                @foreach ($products as $product)
                    <tr>
                        <td>{{ $product->code }}</td>
                        <td>{{ $product->name }}</td>
                        <td>{{ $product->present()->freeShipping }}</td>
                        <td>{{ $product->description }}</td>
                        <td>{{ $product->present()->pricePresent }}</td>
                        <td>{{ $product->category }}</td>
                        <td>
                            <a href="{{ route('products.edit', ['products' => $product->id]) }}" class="button">edit</a>
                            {!! Form::model($product, ['class' => 'form-delete', 'method' => 'delete', 'route' => ['products.destroy', $product->id]]) !!}
                                <button class=" button button-danger">del</button>
                            {!! Form::close() !!}
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </section>
@endsection