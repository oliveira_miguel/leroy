@extends('layout')

@section('content')
    <section class="row">
        <div class="col-xs-12">
            <h2>Product update</h2>
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            {!! Form::model($product, ['method' => 'put', 'route' => ['products.update', $product->id]]) !!}
            {!! Form::hidden('id') !!}

            <div class="row">
                <div class="field col-xs-2">
                    {!! Form::label('code', 'Code') !!}
                    {!! Form::text('code', null, ['disabled'=> true, 'class' => 'input']) !!}
                </div>
            </div>

            <div class="row">
                <div class="field col-xs-6">
                    {!! Form::label('name', 'Name') !!}
                    {!! Form::text('name', null, ['class' => 'input']) !!}
                </div>
            </div>

            <div class="row">
                <div class="field col-xs-2">
                    {!! Form::label('category', 'Category') !!}
                    {!! Form::text('category', null, ['class' => 'input']) !!}
                </div>

                <div class="field col-xs-2">
                    {!! Form::label('price', 'Price $') !!}
                    {!! Form::text('price', null, ['class' => 'input']) !!}
                </div>

                <div class="field col-xs-2">
                    <div class="field input-button checkbox-free-shipping">
                        {!! Form::hidden('free_shipping', 0) !!}
                        {!! Form::checkbox('free_shipping', 1, null, ['id' => 'free_shipping']) !!}
                        {!! Form::label('free_shipping', 'Free Shipping', ['class' => 'label']) !!}
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="field col-xs-6">
                    {!! Form::label('description', 'Description') !!}
                    {!! Form::text('description', null, ['class' => 'input']) !!}
                </div>
            </div>

            <div class="row">
                <div class="field col-xs-6">
                    {!!  Form::submit('Save', ['class' => 'button button-primary']) !!}
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </section>
@endsection